"""Python filesystem-based cache ported from ruby"""
import os
import time
from md5 import md5
try:
    import CPickle as pickle
except ImportError:
    import pickle
def translate_key_to_digest(key):
    """Function translates key to md5 hexdigest unique"""
    return md5(pickle.dumps(key)).hexdigest()
class FileCache:
    """File cache class itself"""
    def __init__(self, domain = 'default', root_dir = '/tmp',
                 expiry = 0, depth = 2):
        self.domain = domain
        self.root_dir = os.path.abspath(root_dir)
        self.expiry = expiry
        self.depth = depth
    def get(self, key):
        """Cache get value by key"""
        path = self.__cache_file_path(key)
        if os.access(path, os.F_OK):
            filep = open(path, 'rb')
            obj = pickle.load(filep)
            filep.close()
            if obj['expire_time'] == 0 or obj['expire_time'] > time.time():
                return obj['value']
            else:
                os.remove(path)
                return None
        else:
            return None
    def set(self, key, value):
        """Cache set function"""
        path = self.__cache_file_path(key)
        obj = self.__create_cache_obj(value)
        if not os.access(os.path.dirname(path), os.F_OK):
            os.makedirs(os.path.dirname(path))
        filep = open(path, 'wb')
        pickle.dump(obj, filep)
        filep.close()    
    def __create_cache_obj(self, obj):
        """Creates cache object which encapsulates expiry param"""
        expire_time = 0
        if self.expiry != 0:
            expire_time = time.time() + self.expiry
        return {'expire_time': expire_time, 'value': obj}
    def __cache_file_path(self, key):
        """Get cache file path in cache files directory tree"""
        key = translate_key_to_digest(key)
        parts = []        
        parts.append(self.root_dir)
        parts.append(self.domain)
        parts.extend(list(key)[0:self.depth])
        parts.append(key)
        return "/".join(parts)
